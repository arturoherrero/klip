# Klip

Klip takes the highlights and notes that you've created on your Kindle and
outputs them into pretty HTML files.


## Example of the HTML files generated

| Index | Book |
| ------ | ------ |
| ![Example of the index file](resources/index.png) | ![Example of a book file](resources/book.png) |


## What input does it support?

Klip uses the content of `My Clippings.txt` file from the Kindle device. You
have to connect the Kindle via USB to extract that file.


## Usage

After moving the clippings text file into the project, run the script:

```sh
$ bin/klip
```


## Project design

Klip could be considered a fork of [Klipbook][1], concretely from https://github.com/grassdog/klipbook/commit/6d60e69.

I started using that project but there were a number of things I didn't like,
so I've accommodated it to my personal preference:
1. Index page
    - I have created an index page listing ALL the books.
    - The HTML files have a favicon 🧐.
2. No options
    - Klip gives no options. You don't need to specify any command, input or
      output files/directories, format, number of books, etc. just run the
      script.
    - It will open the index page in the browser – assuming macOS.
    - It overwrites any existing output files.
    - An option for the user is a decision that the designer was unable to
      make. Hick's Law as a design principle.
3. Re-organize code
    - I have removed some features that I will not use, such as Markdown or
      JSON outputs.
    - Therefore, I have cleaned up a lot of code in the project, moved files as
      I felt it made sense in the project and even eliminated good design
      choices such as dependency injection. Be kind to me.
    - I also removed the entire test suite, aha!
4. No dependencies
    - This is not a Ruby gem.
    - No external dependencies (gems) need to be installed, just Ruby.


## Who made this?

This was made by Arturo Herrero under the MIT License. You can also find me on
Twitter [@ArturoHerrero][2].


[1]: https://github.com/grassdog/klipbook
[2]: https://twitter.com/ArturoHerrero
