module Klip
  Book = Struct.new(:title, :author, :last_update, :clippings) do
    def title_and_author
      author_txt = author ? " by #{author}" : ''
      "#{title}#{author_txt}"
    end

    def sorted_clippings
      clippings.sort_by(&:location)
    end

    def get_binding
      binding
    end
  end
end
