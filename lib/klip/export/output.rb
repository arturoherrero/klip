require 'erb'

module Klip
  class Output
    OUTPUT_DIR = 'output'.freeze

    def call
      input = Input.new
      books = input.books

      export_index(books)
      books.each do |book|
        export_book(book)
      end
    end

    private

    def export_index(books)
      filepath = File.join(OUTPUT_DIR, 'index.html')

      File.open(filepath, 'a') do |file|
        file.write(render_index_content(books))
      end
    end

    def export_book(book)
      filepath = File.join(OUTPUT_DIR, "#{book.title_and_author}.html")

      File.open(filepath, 'w') do |file|
        file.write(render_book_content(book))
      end
    end

    def render_index_content(books)
      count = books.size
      content = books.map do |book|
        %(<li><a href="#{book.title_and_author}.html">#{book.title_and_author}</a></li>)
      end.join

      ERB.new(index_template, 0, '%<>').result(binding)
    end

    def render_book_content(book)
      ERB.new(book_template, 0, '%<>').result(book.get_binding)
    end

    def index_template
      @index_template ||= File.read(File.join(__dir__, 'index.erb'))
    end

    def book_template
      @book_template ||= File.read(File.join(__dir__, 'book.erb'))
    end
  end
end
