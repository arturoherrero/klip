module Klip
  class Input
    INPUT_FILE = 'clippings.txt'.freeze

    def initialize
      @file_text = File.read(INPUT_FILE).strip
      @file_parser = FileParser.new
    end

    def books
      @books ||= build_books
    end

    private

    def build_books
      sorted_entries = extract_sorted_entries_from_file_text
      build_sorted_book_list(sorted_entries)
    end

    def extract_sorted_entries_from_file_text
      entries = @file_parser.extract_entries(@file_text)
      entries.sort { |entry_a, entry_b| entry_a.title <=> entry_b.title }
    end

    def build_sorted_book_list(sorted_entries)
      books_from_entries(sorted_entries).sort do |book_a, book_b|
        book_b.last_update <=> book_a.last_update
      end
    end

    def books_from_entries(entries)
      entries.select { |entry| entry.type != :bookmark }
             .group_by(&:title)
             .map { |_title, book_entries| book_from_entries(book_entries) }
    end

    def book_from_entries(entries)
      entries.sort! { |entry_a, entry_b| entry_a.location <=> entry_b.location }

      Book.new.tap do |book|
        book.title = entries.first.title
        book.author = entries.first.author
        book.last_update = entries.map(&:added_on).max
        book.clippings = entries.map do |entry|
          Clipping.new.tap do |clipping|
            clipping.text = entry.text
            clipping.type = entry.type
            clipping.page = entry.page
            clipping.location = entry.location
          end
        end
      end
    end
  end
end
